import org.apache.commons.codec.digest.DigestUtils;
import java.util.UUID;

public class Main {
	public static void main(String[] args) {
		int n = 1000000;
		// hash 128 bit 1M times
		
		String m128 = randomString(128);
		System.out.println("SHA1 128 bit Message : " + runSHA1(m128, n) + " ms");
		System.out.println("SHA256 128 bit Message : " + runSHA256(m128, n) + " ms");
		System.out.println("SHA512 128 bit Message : " + runSHA512(m128, n) + " ms");
		// hash 256 bit 1M times
		System.out.println("=========================================================");
		String m256 = randomString(256);
		System.out.println("SHA1 256 bit Message : " + runSHA1(m256, n) + " ms");
		System.out.println("SHA256 256 bit Message : " + runSHA256(m256, n) + " ms");
		System.out.println("SHA512 256 bit Message : " + runSHA512(m256, n) + " ms");
		// hash 512 bit 1M times
		System.out.println("=========================================================");
		String m512 = randomString(512);
		System.out.println("SHA1 512 bit Message : " + runSHA1(m512, n) + " ms");
		System.out.println("SHA256 512 bit Message : " + runSHA256(m512, n) + " ms");
		System.out.println("SHA512 512 bit Message : " + runSHA512(m512, n) + " ms");
		// hash 1024 bit 1M times
		System.out.println("=========================================================");
		String m1024 = randomString(1024);
		System.out.println("SHA1 1024 bit Message : " + runSHA1(m1024, n) + " ms");
		System.out.println("SHA256 1024 bit Message : " + runSHA256(m1024, n) + " ms");
		System.out.println("SHA512 1024 bit Message : " + runSHA512(m1024, n) + " ms");
	}
	
	public static long runSHA1(String plain, int n) {
		long start = System.currentTimeMillis();
		for(int i = 0; i<n; i++) {
			DigestUtils.sha1(plain);
		}
		long end = System.currentTimeMillis();
		return end - start;
	}
	
	public static long runSHA256(String plain, int n) {
		long start = System.currentTimeMillis();
		for(int i = 0; i<n; i++) {
			DigestUtils.sha256(plain);
		}
		long end = System.currentTimeMillis();
		return end - start;
	}
	
	public static long runSHA512(String plain, int n) {
		long start = System.currentTimeMillis();
		for(int i = 0; i<n; i++) {
			DigestUtils.sha512(plain);
		}
		long end = System.currentTimeMillis();
		return end - start;
	}
	
	public static String randomString(int n) {
		String result = "";
		n = n / 128;
		for (int i = 0; i<n; i++){
			result+=UUID.randomUUID().toString();
		}
		return result;
	}
}


